<?php

namespace App\Http\Controllers;


use App\Http\Controllers\Controller;
use GuzzleHttp\Client;
class RobberyController extends Controller
{
  
    public function robberyTookPlace()
    {
        $data = base64_encode(file_get_contents( $_FILES["photo"]["tmp_name"] ));
    
    $client = new Client();
    $r = $client->request('POST', 'https://vision.googleapis.com/v1/images:annotate?key=AIzaSyDtHCtSeeyWnX7Q9LOlMU0uTZAOxVPzQ5s', [
    'body' => '{
  "requests":[
    {
      "image":{
             
        "content": "'.$data.'"
      },
      "features":[
        {
          "type":"WEB_DETECTION",
          "maxResults":10
        }
      ]
    }
  ]
}

'
]);

$body = $r->getBody();
$array = json_decode($r->getBody(), true);
$listRobbery=array("Robbery", "Theft", "Bank robbery", "Crime", "Knife", "Arrest", "Hood", "Weapon", "Firearm", "Gun", "Hammer", "Fight", "Extortion");

$size=count($array['responses'][0]['webDetection']['webEntities']);
$exist="false";
$description="";
for($i=0; $i<$size ;$i++){
    $word=$array['responses'][0]['webDetection']['webEntities'][$i]['description'];
    if(in_array($word, $listRobbery)){
        $exist="true";
        if($description==""){
        $description=$word;
        }
        else
        $description=$description.', '.$word;   
    }
}
$jsonReturn=array('Robbery' => $exist, 'Description' => $description);
echo json_encode($jsonReturn);
    }
}
